
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica41 {
    public static void main(String[] args) {
        Elipse elip = new Elipse(8.0,4.0);
        Circulo circ = new Circulo(8);
        System.out.println("A área da elipse é = "+elip.getArea());
        System.out.println("O perímetro da elipse é = "+elip.getPerimetro());
        System.out.println("A área do círculo é = "+elip.getArea());
        System.out.println("O perímetro do círculo = "+circ.getPerimetro());
    }
}
