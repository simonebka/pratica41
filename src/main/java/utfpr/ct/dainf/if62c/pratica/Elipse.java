/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Simone
 */
public class Elipse {
    public double eixo1=0, eixo2=0;
   
    public  Elipse(){
        this.eixo1=0.0;
        this.eixo2=0.0;
    }
    public  Elipse(double e1, double e2){
        this.eixo1=e1;
        this.eixo2=e2;
    }
    public double getArea(){
        double a=0;
        a = Math.PI*(eixo1/2)*(eixo2/2);
        return(a);
    }
    public double getPerimetro(){
        double p=0,r=0,s=0;
        r=eixo1/2;
        s=eixo2/2;
        p=Math.PI*(3*(r+s)-Math.sqrt((3*r+s)*(r+3*s)));
        return(p);
    }
}
